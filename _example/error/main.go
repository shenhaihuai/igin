package main

import (
	"gitlab.com/shenhaihuai/igin/xerror"

	"github.com/gin-gonic/gin"
	"gitlab.com/shenhaihuai/igin"
	"gitlab.com/shenhaihuai/igin/middleware"
)

func main() {
	g := igin.Default()
	g.Use(func(context *gin.Context) {
		//igin.AddStatusError(context, errors.NewHTTPError(201, "test error"))
		igin.AddStatusError(context, xerror.NewCodeMsg(201, "test error"))
		//igin.AddStatusError(context, fmt.Errorf("test error"))
		//igin.AddStatusError(context, fmt.Errorf("test error"), 202)
	})
	g.Use(middleware.ErrorsNext())
	g.GET("/", func(context *gin.Context) {
		context.String(200, "ok")
	})
	g.Run()
}
