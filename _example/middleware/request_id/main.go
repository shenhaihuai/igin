package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/shenhaihuai/igin/middleware"
)

func main() {
	g := gin.Default()
	g.Use(middleware.RequestIdNext())
	g.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "ok")
	})
	g.Run()
}
