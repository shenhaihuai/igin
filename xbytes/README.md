## Installation

```go
go get gitlab.com/shenhaihuai/igin/xbytes
```

## [Usage](https://gitlab.com/shenhaihuai/igin/blob/main/xbytes/bytes_test.go)

### Format

```go
fmt.Println(xbytes.Format(13231323))
```

`12.62MB`

### Parse

```go
b, _ = xbytes.Parse("2M")
fmt.Println(b)
```

`2097152`