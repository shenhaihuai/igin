package xslices

// Remove removes given strs from strings.
func Remove[T comparable](slices []T, ts ...T) []T {
	out := append([]T(nil), slices...)
	for _, t := range ts {
		var n int
		for _, v := range out {
			if v != t {
				out[n] = v
				n++
			}
		}
		out = out[:n]
	}
	return out
}

// Contains checks if str is in list.
func Contains[T comparable](slices []T, values ...T) bool {
	set := make(map[T]bool, 0)
	for _, i := range slices {
		set[i] = true
	}
	for _, value := range values {
		if _, ok := set[value]; !ok {
			return false
		}
	}
	return true
}
func Concatenate[T any](s []T, objs ...[]T) []T {
	for i := 0; i < len(objs); i++ {
		for j := 0; j < len(objs[i]); j++ {
			s = append(s, objs[i][j])
		}
	}
	return s
}

func Filter[T any](slice []T, fun func(item T) bool) []T {
	var s []T
	for _, item := range slice {
		if fun(item) {
			s = append(s, item)
		}
	}
	return s
}
func Pluck[T any, V comparable](s []T, getValue func(item T) V) []V {
	var ks []V
	for _, t := range s {
		ks = append(ks, getValue(t))
	}
	return ks
}
func IndexF[T comparable](s []T, fn func(T) string) map[string]T {
	maps := make(map[string]T, len(s))
	for _, item := range s {
		maps[fn(item)] = item
	}
	return maps
}

func UniqueF[T comparable](s []T, fn func(T) string) []T {
	maps := IndexF(s, fn)
	ns := make([]T, 0)
	for _, t := range maps {
		ns = append(ns, t)
	}
	return ns
}

func ColumnF[T comparable, V comparable](s []T, fn func(T) V) []V {
	ns := make([]V, 0)
	for _, t := range s {
		ns = append(ns, fn(t))
	}
	return ns
}

func Merge[T comparable](s []T, ts ...T) []T {
	for _, t := range ts {
		s = append(s, t)
	}
	return s
}
