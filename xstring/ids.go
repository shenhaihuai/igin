package xstring

import (
	"strconv"
	"strings"
)

var (
	IntSliceSymbol = ","
)

//IntStringToSlice 字符转int切片 1,2,3=>[]int{1,2,3}
func IntStringToSlice(si string) []int {
	var ss []int
	for _, s := range strings.Split(si, IntSliceSymbol) {
		if sInt, err := strconv.Atoi(s); err == nil {
			ss = append(ss, sInt)
		}
	}
	return ss
}

// IntSliceToString  []int{1,2,3}=>1,2,3
func IntSliceToString(slices []int) string {
	strSlice := make([]string, len(slices))
	for i, num := range slices {
		strSlice[i] = strconv.Itoa(num)
	}
	return strings.Join(strSlice, IntSliceSymbol)
}
